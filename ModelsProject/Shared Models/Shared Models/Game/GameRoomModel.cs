﻿namespace SharedModels.Game
{
    public class GameRoomModel
    {
        public int? Id { get; set; }
        public PlayerModel Player1 { get; set; }
        public PlayerModel Player2 { get; set; }
        public int RoomStatus { get; set; }
    }
}