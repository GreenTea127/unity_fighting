﻿using SharedModels.Enums;

namespace SharedModels.Game
{
    public class GameRoomUnityModel
    {
        public PlayerModel Player1 { get; set; }
        public PlayerModel Player2 { get; set; }
        public GameRoomStatus RoomStatus { get; set; }
        public byte PlayersCount { get; set; }
        public bool IsFull { get; set; }
    }
}
