﻿namespace SharedModels.Game
{
    public class PlayerModel
    {
        public int? Id { get; set; }
        public string PlayerName { get; set; }
    }
}
