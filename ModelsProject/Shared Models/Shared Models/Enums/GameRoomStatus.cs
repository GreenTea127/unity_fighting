﻿namespace SharedModels.Enums
{
    public enum GameRoomStatus
    {
        Waiting,
        InBattle
    }
}
