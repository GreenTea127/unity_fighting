﻿
namespace GameServer.Models.ViewModels
{
    public class PlayerForRoomViewModel
    {
        public int? Id { get; set; }
        public string PlayerName { get; set; }
    }
}