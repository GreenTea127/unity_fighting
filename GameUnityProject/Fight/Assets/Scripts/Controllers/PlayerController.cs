﻿using UnityEngine;
using System.Linq;
using Assets.Scripts.SourceCode.Models;
using Assets.Scripts.SourceCode;

namespace Develop
{
    public class PlayerController : MonoBehaviour
    {

        Transform _transform;
        Rigidbody2D _rg;
        Animator anim;

        float horizontal;
        float vertical;

        public float maxSpeed = 25;
        bool crouch;

        public float JumpForce = 20;
        public float JumpDuration = .1f;
        float jmpDuration;
        float _jmpForce;
        bool jumpKey;
        bool falling;
        bool onGround;

        public float atackRate = .3f;
        bool[] atack = new bool[2];
        float[] atackTimer = new float[2];
        ushort[] timesPressed = new ushort[2];

        bool block;

        void Start()
        {
            _transform = GetComponent<Transform>();
            _rg = GetComponent<Rigidbody2D>();
            anim = GetComponentInChildren<Animator>();
        }

        void FixedUpdate()
        {
            horizontal = Input.GetAxis("Horizontal");
            vertical = Input.GetAxis("Vertical");

            Vector2 movement = new Vector2(horizontal, 0);
            var animationInfo = anim.GetCurrentAnimatorClipInfo(0).First();

            if (animationInfo.clip.name == "Punch")
            {
                horizontal = 0f;
                _rg.velocity = Vector2.zero;
            }

            crouch = (vertical < -0.1f);

            if (vertical > 0.1f)
            {
                if (!jumpKey)
                {
                    jmpDuration += Time.deltaTime;
                    _jmpForce += Time.deltaTime;

                    if (jmpDuration < JumpDuration)
                    {
                        _rg.velocity = new Vector2(_rg.velocity.x, _jmpForce);
                    }
                    else
                    {
                        jumpKey = true;
                    }
                }
            }

            if (!onGround && vertical < .1f)
            {
                falling = true;
            }

            if (!crouch)
            {
                if (movement.x != 0)
                {
                    Vector3 currentTransform = _transform.position;
                    var currentX = currentTransform.x;
                    var currentY = currentTransform.y;
                    var currentZ = currentTransform.z;

                    var newPosition = new Vector3(
                        currentX + movement.x * maxSpeed,
                        currentY,
                        currentZ);

                    _transform.position = newPosition;
                }

            }
            else
            {
                _rg.velocity = Vector2.zero;
            }


        }

        private void Update()
        {
            Block();
            AtackInput(0);
            AtackInput(1);
            UpdateAnimator();
            OnGroundCheck();
        }

        private void LateUpdate()
        {
            var model = _CreateCurrentPlayerModel();
            ServerHelper.SendMessage(model);
        }


        void UpdateAnimator()
        {
            anim.SetBool("isBlock", block);
            anim.SetBool("Crouch", crouch);
            anim.SetBool("OnGround", this.onGround);
            anim.SetBool("Falling", this.falling);
            anim.SetFloat("Movement", Mathf.Abs(horizontal));

            anim.SetBool("Attack0", atack[0]);
            anim.SetBool("Attack1", atack[1]);
        }


        void AtackInput(short atackNumberIndex)
        {
            if (Input.GetButtonDown("Attack" + atackNumberIndex))
            {
                atack[atackNumberIndex] = true;
                atackTimer[atackNumberIndex] = 0;
                timesPressed[atackNumberIndex]++;
            }

            if (atack[atackNumberIndex])
            {
                atackTimer[atackNumberIndex] += Time.deltaTime;

                if (atackTimer[atackNumberIndex] > atackRate || timesPressed[atackNumberIndex] >= 4)
                {
                    atackTimer[atackNumberIndex] = 0;
                    atack[atackNumberIndex] = false;
                    timesPressed[atackNumberIndex] = 0;
                }
            }
        }


        void Block()
        {
            if (Input.GetButtonDown("Block"))
            {
                block = true;
            }

            if (Input.GetButtonUp("Block"))
            {
                block = false;
            }
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.collider.tag == "Ground")
            {
                onGround = true;
                jumpKey = false;
                jmpDuration = 0;
                _jmpForce = JumpForce;
                falling = false;

            }
        }

        void OnGroundCheck()
        {
            if (!onGround)
            {
                _rg.gravityScale = 5;
            }
            else
            {
                _rg.gravityScale = 1;
            }
        }

        private void OnCollisionExit2D(Collision2D collision)
        {
            if (collision.collider.tag == "Ground")
            {
                onGround = false;
            }
        }


        private PlayerModel _CreateCurrentPlayerModel()
        {
            var model = new PlayerModel
            {
                IsAttack1 = atack[0],
                IsAttack2 = atack[1],
                IsBlock = block,
                IsCrouch = crouch,
                IsFalling = falling,

                Position = new Vector2Model
                {
                    X = _transform.position.x,
                    Y = _transform.position.y
                },
                IsMoving = Mathf.Abs(horizontal),
                IsGround = onGround
            };

            return model;
        }
    }
}

