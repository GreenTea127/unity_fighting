﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{

    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

    public void LoadRoomScene()
    {
        _LoadSceneByIndex(1);
    }

    public void LoadLoginScene()
    {
        _LoadSceneByIndex(0);
    }

    public void LoadFightScene()
    {
        _LoadSceneByIndex(2);
    }

    public static void _LoadSceneByIndex(int index)
    {
        SceneManager.LoadScene(index, LoadSceneMode.Single);
    }
}
