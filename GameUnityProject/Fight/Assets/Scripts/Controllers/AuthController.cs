﻿using UnityEngine;
using UnityEngine.UI;
using System.Net;
using System;
using RestSharp;
using Microsoft.AspNet.SignalR.Client;

public class AuthController : MonoBehaviour
{
    public Text Login;
    public Text Password;
    public string SiteUrl;

    public GameObject SceneSwitcher;

    private SceneController _sceneController;
    private Cookie _authCookie;


    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
        _sceneController = SceneSwitcher.GetComponent<SceneController>();
        
    }

    public void SendRequest()
    {
        var connection = new HubConnection(SiteUrl);

        var authResult = _SendRequest(Login.text, Password.text, SiteUrl);

        if (authResult)
        {
            connection.CookieContainer = new CookieContainer();
            connection.CookieContainer.Add(_authCookie);
            _sceneController.LoadRoomScene();
        }
        else
        {
            return;
        }
        
        
    }

    private bool _SendRequest(string login, string password, string siteUrl)
    {
        var client = new RestClient(siteUrl);
        var _cookieContainer = new CookieContainer();
        client.CookieContainer = _cookieContainer;

        var request = new RestRequest("Auth/Login", Method.POST);
        request.AddJsonBody(new { Login = login, Password = password });
        IRestResponse response = client.Post(request);

        var uri = new Uri(siteUrl);
        CookieCollection responseCookies = _cookieContainer.GetCookies(uri);

        if (responseCookies.Count == 0)
        {
            _authCookie = null;
            return false;
        }

        _authCookie = responseCookies[".AspNet.ApplicationCookie"];
        return true;
    }
}
