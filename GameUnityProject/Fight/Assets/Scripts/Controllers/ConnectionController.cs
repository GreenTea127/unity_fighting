﻿using Assets.Scripts.SourceCode;
using UnityEngine;

public class ConnectionController : MonoBehaviour {

    public string ServerUrl;

    private void Awake()
    {
        ServerHelper.InitConnection(ServerUrl);
    }
}
