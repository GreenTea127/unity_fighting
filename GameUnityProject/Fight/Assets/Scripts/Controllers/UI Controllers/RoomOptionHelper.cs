﻿using Assets.Scripts.SourceCode;
using Assets.Scripts.SourceCode.GameRoom;
using SharedModels.Game;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class RoomOptionHelper : MonoBehaviour
{

    public GameObject OptionPrefab;
    public GameObject ContentTarget;
    public float TopOffset;

    



    public void BuildGrid()
    {
        _ClearContent();
        string rooms = ServerHelper.GetRoomState();

        IReadOnlyList<GameRoomModel> list = new List<GameRoomModel>
        {
            new GameRoomModel
            {
                Player1 = new PlayerModel
                {
                    Id = 0,
                    PlayerName = "player1"
                },
                Player2 = new PlayerModel
                {
                    Id = 1,
                    PlayerName = "player2"
                },
                RoomStatus = 0,
                Id = 0
            },
            new GameRoomModel
            {
                Player1 = new PlayerModel
                {
                    Id = 2,
                    PlayerName = "player3"
                },
                Player2 = new PlayerModel
                {
                    Id = 3,
                    PlayerName = "player4"
                },
                RoomStatus = 1,
                Id = 1
            },
            new GameRoomModel
            {
                Player1 = new PlayerModel
                {
                    Id = 0,
                    PlayerName = "player1"
                },
                RoomStatus = 0,
                Id = 2
            },
        };
        var mapper = new PlayerModelMapper();
        IReadOnlyList<GameRoomUnityModel> convertedModels = list
                                                                .Select(m => mapper.MapPlayerObject(m))
                                                                .ToList();
        var gameObjects = _CreateRoomOptions(convertedModels);

        foreach (var item in gameObjects)
        {
            item.transform.SetParent(ContentTarget.transform);
            var itemRectTrasnform = item.GetComponent<RectTransform>();
        }

        StartCoroutine(StartReplaceElements());
    }

    private IEnumerator StartReplaceElements()
    {
        yield return new WaitForEndOfFrame();
        IReadOnlyList<GameObject> menuOptions = _GetExistMenuOptions();

        for (int i =0, length = menuOptions.Count; i < length; i ++)
        {
            var item = menuOptions[i];

            var rectTransform = item.GetComponent<RectTransform>();

            float offsetHeight = (TopOffset + rectTransform.rect.height * i) * (-1f);
            rectTransform.anchoredPosition =new Vector2(0f, offsetHeight);
        }
        yield return null;
    }

    private void _ClearContent()
    {
        IReadOnlyList<GameObject> menuOptions = _GetExistMenuOptions();

        for (int i = 0, length = menuOptions.Count; i < length; i++)
        {
            var item = menuOptions[i];
            Destroy(item);
        }
    }

    private IReadOnlyList<GameObject> _GetExistMenuOptions()
    {
        return GameObject.FindGameObjectsWithTag("RoomOptionPrefab").ToList();
    }

    private IReadOnlyList<GameObject> _CreateRoomOptions(IReadOnlyList<GameRoomUnityModel> roomModels)
    {
        List<GameObject> resultList = new List<GameObject>();

        foreach (var i in roomModels)
        {
            var gameObject = Instantiate(OptionPrefab) as GameObject;

            string hostName = _GetHostName(i);

            var textElementList = gameObject.GetComponentsInChildren<Text>().ToList();
            //textElement.text = hostName;

            textElementList.First(item => item.name == "Players Count").text = $"Players Count: {i.PlayersCount}/2";
            textElementList.First(item => item.name == "Leader Name").text = $"Leader Name: {i.Player1.PlayerName}";

            resultList.Add(gameObject);

            if(i.PlayersCount >= 2)
            {
                var obj = gameObject.transform.Find("Join Room");
                obj.gameObject.SetActive(false);//GetComponentInChildren<Button>().enabled = false;
            }
        }

        return resultList;
    }

    private string _GetHostName(GameRoomUnityModel model)
    {
        if (model.Player1 != null && !string.IsNullOrEmpty(model.Player1.PlayerName))
        {
            return model.Player1.PlayerName;
        }

        if (model.Player2 != null && !string.IsNullOrEmpty(model.Player2.PlayerName))
        {
            return model.Player2.PlayerName;
        }

        return "Unknow";
    }
}
