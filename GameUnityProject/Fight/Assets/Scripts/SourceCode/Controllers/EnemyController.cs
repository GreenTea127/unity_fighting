﻿using Assets.Scripts.SourceCode;
using Assets.Scripts.SourceCode.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{

    Animator _animator;
    Transform _transform;

    float horizontal;
    bool crouch;

    bool falling;
    bool onGround;

    bool[] atack = new bool[2];
    bool block;

    void Start()
    {
        _animator = GetComponentInChildren<Animator>();
        _transform = GetComponent<Transform>();
    }

    void UpdateAnimator()
    {
        _animator.SetBool("isBlock", block);
        _animator.SetBool("Crouch", crouch);
        _animator.SetBool("OnGround", this.onGround);
        _animator.SetBool("Falling", this.falling);
        _animator.SetFloat("Movement", Mathf.Abs(horizontal));

        _animator.SetBool("Attack0", atack[0]);
        _animator.SetBool("Attack1", atack[1]);
    }

    void Update()
    {
        PlayerModel model = ServerHelper.GetPlayerState();

        if (model != null)
        {
            horizontal = model.IsMoving;
            crouch = model.IsCrouch;
            block = model.IsBlock;
            atack[0] = model.IsAttack1;
            atack[1] = model.IsAttack2;
            falling = model.IsFalling;
            onGround = model.IsGround;

            //to do: remove additional distance
            _transform.position = new Vector2(model.Position.X + 10, model.Position.Y);
        }
        UpdateAnimator();
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.name == "DamageCollider")
        {
            _animator.SetTrigger("isDamaged");
        }
    }
}
