﻿using Newtonsoft.Json;
using SharedModels.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.SourceCode.GameRoom
{
    public class RoomModelJsonParser
    {
        public List<GameRoomModel> GetRoomModelListFromJson(string responseJson)
        {
            var deserializedList = JsonConvert.DeserializeObject<List<GameRoomModel>>(responseJson);
            return deserializedList;
        }
    }
}
