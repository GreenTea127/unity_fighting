﻿using SharedModels.Enums;
using SharedModels.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Assets.Scripts.SourceCode.GameRoom
{
    public class PlayerModelMapper
    {
        public GameRoomUnityModel MapPlayerObject(GameRoomModel model)
        {
            byte playerCount = 0;
            bool isFull;

            if (model.Player1 != null) playerCount++;
            if (model.Player2 != null) playerCount++;

            isFull = playerCount == 2;

            var roomStatus = (GameRoomStatus)model.RoomStatus;

            var unityRoomModel = new GameRoomUnityModel
            {
                Player1 = model.Player1,
                Player2 = model.Player2,
                IsFull = isFull,
                RoomStatus = roomStatus,
                PlayersCount = playerCount
            };

            return unityRoomModel;
        }
    }
}
