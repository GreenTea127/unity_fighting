﻿using Assets.Scripts.SourceCode.Models;
using Microsoft.AspNet.SignalR.Client;
using Newtonsoft.Json;
using RestSharp;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Assets.Scripts.SourceCode
{
    public static class ServerHelper
    {
        private static IHubProxy _testGameHub;

        private static ConcurrentQueue<string> StateQueue;


        static ServerHelper()
        {
            StateQueue = new ConcurrentQueue<string>();
        }

        public static void InitConnection(string serverUrl)
        {
            HubConnection hubConnection = new HubConnection(serverUrl);
            _testGameHub = hubConnection.CreateHubProxy("TestGameHub");


            hubConnection.Start().Wait();

            _testGameHub.On<string>("SentActions", jsonString =>
            {
                string json = jsonString;
                StateQueue.Enqueue(json);
            });

        }

        public static string GetRoomState()
        {

            var request = new RestRequest("api/gamerooms/getall", Method.POST);
            var client = new RestClient("http://localhost:61410");
            IRestResponse response = client.Post(request);
            return "";
        }

        public static PlayerModel GetPlayerState()
        {
            if(StateQueue.IsEmpty)
            {
                return null;
            }

            string json;

            if (StateQueue.TryDequeue(out json))
            {
                var deserializedModel = JsonConvert.DeserializeObject<PlayerModel>(json);
                return deserializedModel;
            }
            else
            {
                return null;
            }
        }

        public static void SendMessage(PlayerModel playerModel)
        {
            string json = JsonConvert.SerializeObject(playerModel);
            _testGameHub.Invoke("DoActions", json);
        }
    }
}
