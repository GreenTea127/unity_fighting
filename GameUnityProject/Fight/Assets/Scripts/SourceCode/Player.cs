﻿//using UnityEngine;
//using System.Collections;
//using System;

//public class Player
//{
//    private Animator _animator;
//    private Rigidbody2D _rb;

//    #region Animation state names
//    public const string _blockFieldName = "isBlock";
//    public const string _damagedFieldName = "isDamaged";
//    public const string _downFieldName = "IsDown";
//    public const string _kickFieldName = "Kick";
//    public const string _punchFieldName = "Punch";
//    public const string _moveFieldName = "isMoving";
//    #endregion

//    #region Atack action states
//    private bool punch;
//    private bool kick;
//    private bool _isBlocking;
//    #endregion

//    #region Move action states
//    private bool _isMoving;
//    private bool _isSitDown;
//    #endregion

//    private float _moveSpeed;

//    public Player(Animator animator, Rigidbody2D rigidbody, float atackRate, float moveSpeed)
//    {
//        if (animator == null)
//        {
//            throw new NullReferenceException("Animator object is null");
//        }

//        _moveSpeed = moveSpeed;

//        _animator = animator;
//        _rb = rigidbody;
//    }

//    public void Update(float horizontal, float vertical, bool punch, bool kick, float block)
//    {
//        //You cannot move and atack at the same time so we need to check atack actions state
//        if (punch || kick || block > 0)
//        {
//            _OnPunch(punch);
//            _OnKick(kick);

//            _OnSitDown(0f);
//            _OnMove(0f);
//            _rb.velocity = Vector2.zero;
//        }
//        else
//        {
//            _OnSitDown(vertical);
//            _OnMove(horizontal);
//        }

//        _OnBlock(block);

//        UpdateAnimator();
//    }

//    private void UpdateAnimator()
//    {
//        _animator.SetBool(_moveFieldName, _isMoving);
//        _animator.SetBool(_downFieldName, _isSitDown);
//        _animator.SetBool(_blockFieldName, _isBlocking);
//    }

//    private void _OnMove(float move)
//    {

//        if (move == 0 || _isSitDown)
//        {
//            _isMoving = false;
//            _rb.velocity = Vector2.zero;
//        }
//        else
//        {
//            _isMoving = true;

//            var movingVector = new Vector2(Mathf.Sign(move) * _moveSpeed, 0);
//            _rb.AddForce(movingVector);
//        }
//    }

//    private void _OnSitDown(float sitDown)
//    {
//        _isSitDown = sitDown < 0;
//    }

//    private void _OnPunch(bool punch)
//    {
//        if (punch)
//        {
//            _animator.SetTrigger(_punchFieldName);
//        }
//    }

//    private void _OnKick(bool kick)
//    {
//        if (kick)
//        {
//            _animator.SetTrigger(_kickFieldName);
//        }
//    }

//    private void _OnBlock(float block)
//    {
//        _isBlocking = block > 0;
//    }
//}
