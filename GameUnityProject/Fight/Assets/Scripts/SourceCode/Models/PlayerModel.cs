﻿namespace Assets.Scripts.SourceCode.Models
{
    public class PlayerModel
    {
        public Vector2Model Position { get; set; }
        public bool IsBlock { get; set; }
        public bool IsCrouch { get; set; }
        public bool IsFalling { get; set; }
        public bool IsAttack1 { get; set; }
        public bool IsAttack2 { get; set; }
        public float IsMoving { get; set; }
        public bool IsGround { get;  set; }
    }
}
