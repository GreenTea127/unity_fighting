﻿namespace Assets.Scripts.SourceCode.Models
{
    public class Vector2Model
    {
        public float X { get; set; }
        public float Y { get; set; }
    }
}
