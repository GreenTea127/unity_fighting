﻿using Microsoft.AspNet.SignalR.Client;
using System;
using Newtonsoft.Json;

namespace SignalRClientExample
{
    class Program
    {
        static void Main(string[] args)
        {
            string siteUrl = "http://localhost:61410";

            string login = "test";
            string password = "asd";

            HubConnection hubConnection = HubConnectionsFactory.GetAuthorizedHubConnection(siteUrl, login, password);

            if (hubConnection == null)
            {
                Console.WriteLine("Login failed");
                return;
            }

            //IHubProxy chatHub = hubConnection.CreateHubProxy("ChatHub");
            //hubConnection.Start().Wait();

            //chatHub.Invoke("Send", "msg text example");
            //chatHub.On("AddMessage", res =>
            //{
            //    Console.WriteLine(res);
            //});

            //IHubProxy gameHub = hubConnection.CreateHubProxy("GameHub");
            //hubConnection.Start().Wait();

            //gameHub.On("SentRooms", result =>
            //{
            //    Console.WriteLine(result);
            //});

            //gameHub.On("StartBattle", result =>
            //{
            //    Console.WriteLine(result);
            //});

            //gameHub.Invoke("GetRooms").Wait();
            //gameHub.Invoke("JoinRoom", 1).Wait();
            //gameHub.Invoke("IamReady").Wait();
            //gameHab.Invoke("CreateRoom");
            //gameHab.Invoke("LeaveRoom");

            IHubProxy testGameHub = hubConnection.CreateHubProxy("TestGameHub");
           
            testGameHub.On("SentActions", actionsJson =>
            {
                Console.WriteLine(actionsJson);
            });

            hubConnection.Start().Wait();

            testGameHub.Invoke("DoActions", "test msg");

            Console.WriteLine("Press Enter to exit.");
            Console.ReadKey();
        }
    }
}
