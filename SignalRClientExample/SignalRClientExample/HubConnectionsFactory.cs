﻿using Microsoft.AspNet.SignalR.Client;
using System.Net;
using RestSharp;
using System.Linq;
using System;

namespace SignalRClientExample
{
    public static class HubConnectionsFactory
    {
        public static HubConnection GetAuthorizedHubConnection(string siteUrl, string login, string password)
        {
            var connection = new HubConnection(siteUrl);

            Cookie returnedCookie;
            var authResult = _AuthenticateUserAsync(siteUrl, login, password, out returnedCookie);

            if (authResult)
            {
                connection.CookieContainer = new CookieContainer();
                connection.CookieContainer.Add(returnedCookie);
                return connection;
            }
            return null;
        }

        //private static bool _AuthenticateUserAsync(string siteUrl, string login, string password, out Cookie authCookie)
        //{
        //    var cookies = new CookieContainer();
        //    var handler = new HttpClientHandler();
        //    handler.CookieContainer = cookies;

        //    using (var httpClient = new HttpClient(handler))
        //    {
        //        var content = new FormUrlEncodedContent(new[]
        //        {
        //            new KeyValuePair<string, string>("Login", login),
        //            new KeyValuePair<string, string>("Password", password)
        //        });

        //        Task<HttpResponseMessage> responseTask = httpClient.PostAsync($"{siteUrl}/Auth/Login", content);
        //        responseTask.Wait();

        //        var reponse = responseTask.Result;

        //        if (reponse.StatusCode != HttpStatusCode.OK)
        //        {
        //            authCookie = null;
        //            return false;
        //        }

        //        var uri = new Uri(siteUrl);
        //        CookieCollection responseCookies = cookies.GetCookies(uri);

        //        if (responseCookies.Count == 0)
        //        {
        //            authCookie = null;
        //            return false;
        //        }

        //        authCookie = responseCookies[".AspNet.ApplicationCookie"];
        //        return true;
        //    }
        //}

        private static bool _AuthenticateUserAsync(string siteUrl, string login, string password, out Cookie authCookie)
        {
            var client = new RestClient(siteUrl);
            var _cookieContainer = new CookieContainer();
            client.CookieContainer = _cookieContainer;

            var request = new RestRequest("Auth/Login", Method.POST);
            request.AddJsonBody(new { Login = login, Password = password });
            IRestResponse response = client.Post(request);

            var uri = new Uri(siteUrl);
            CookieCollection responseCookies = _cookieContainer.GetCookies(uri);

            if (responseCookies.Count == 0)
            {
                authCookie = null;
                return false;
            }

            authCookie = responseCookies[".AspNet.ApplicationCookie"];
            return true;
        }
    }
}
